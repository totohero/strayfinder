package com.thomas.strayfinder2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class MainActivity extends SherlockListActivity implements
		OnSharedPreferenceChangeListener {
	private AnimalsAdapter animalsAdapter;
	private ProgressBar pb = null;
	private TextView tv = null;
	private PullToRefreshListView lv = null;
	private ChangeLog cl = null;

	private boolean active = false;
	private boolean prefChanged = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		pb = (ProgressBar) findViewById(R.id.progress_bar);
		tv = (TextView) findViewById(R.id.sub_text);
		lv = (PullToRefreshListView) findViewById(R.id.lv_main);

		animalsAdapter = new AnimalsAdapter(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				return getListViewItem(position, convertView, parent);
			}

			public void onStartLoad() {
				pb.setVisibility(View.VISIBLE);
			}

			public void onFinishLoad() {
				super.onFinishLoad();

				lv.onRefreshComplete();
				pb.setVisibility(View.GONE);
			}
		};

		animalsAdapter.registerDataSetObserver(new DataSetObserver() {
			@Override
			public void onChanged() {
				MainActivity.this.updateTitleBar();
				super.onChanged();
			}
		});

		lv.setMode(Mode.PULL_FROM_END);
		lv.getRefreshableView().setAdapter(animalsAdapter);
		lv.setOnRefreshListener(new OnRefreshListener<ListView>() {
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				animalsAdapter.loadMorePage();
			}
		});

		PreferenceManager.getDefaultSharedPreferences(this)
				.registerOnSharedPreferenceChangeListener(this);

		if (MyApp.shouldRefresh())
			lv.setRefreshing(false);

		cl = new ChangeLog(this);
		if (cl.firstRun())
			cl.getLogDialog().show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;

		switch (item.getItemId()) {
		case R.id.menu_settings:
			intent = new Intent(MainActivity.this, SettingsActivity.class);
			MainActivity.this.startActivity(intent);
			break;
		case R.id.menu_refresh:
			refresh();
			break;
		case R.id.menu_about:
			cl.getLogDialog().show();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		active = false;
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		active = true;

		// If prefChanged and shouldRefresh, do refresh
		if (MyApp.shouldRefresh() && prefChanged)
			refresh();

		prefChanged = false;

		updateTitleBar();
	}

	private String oldCondition;

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (MyApp.getSearchCondition().equals(oldCondition))
			return;

		// When activity is not active, just set prefChanged as true and return
		if (!active)
			prefChanged = true;
		else if (MyApp.shouldRefresh())
			refresh();

		oldCondition = MyApp.getSearchCondition();
	}

	private void refresh() {
		animalsAdapter.clearAll();
		lv.setRefreshing(false);
	}

	private void updateTitleBar() {
		int count = (animalsAdapter == null) ? 0 : animalsAdapter.getCount();

		tv.setText(MyApp.getSearchCondition() + " / " + count + "�׸� �˻�");
	}
}