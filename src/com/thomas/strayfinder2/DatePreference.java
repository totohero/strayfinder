package com.thomas.strayfinder2;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

public class DatePreference extends DialogPreference {
	private DatePicker dp;

	public DatePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);

		dp = (DatePicker) view.findViewById(R.id.date_pick);

		String[] dat = MyApp.getSearchDate(this.getKey()).split("-");
		if (dat.length > 2)
			dp.init(Integer.parseInt(dat[0]), Integer.parseInt(dat[1]) - 1,
					Integer.parseInt(dat[2]), null);
		else
			dp.init(2013, 0, 1, null);
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		String date = String.format("%04d-%02d-%02d", dp.getYear(),
				dp.getMonth() + 1, dp.getDayOfMonth());
		if (positiveResult) {
			Log.i("DatePick", "picked p " + date);

			this.persistString(date);
		} else {
			Log.i("DatePick", "picked n " + date);
		}
	}
}
