package com.thomas.strayfinder2;

import java.util.Calendar;
import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

public class MyApp extends Application {
	private static Context app;

	public static Context getApp() {
		return app;
	}

	public static String[] get_s_upr_cd() {
		return PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_upr_cd", "전국,").split(",");
	}

	public static String[] get_s_org_cd() {
		return PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_org_cd", "선택,").split(",");
	}

	public static String[] get_s_shelter_cd() {
		return PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_shelter_cd", "").split(",");
	}

	public static String[] get_s_up_kind_cd() {
		return PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_up_kind_cd", "모든 종,").split(",");
	}

	public static String[] get_s_kind_cd() {
		return PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString("s_kind_cd", "선택,").split(",");
	}

	public static String getSearchCondition() {
		String upr = get_s_upr_cd()[0];
		String org = get_s_org_cd()[0];
		String shelter = get_s_shelter_cd()[0];
		String up_kind = get_s_up_kind_cd()[0];
		String kind = get_s_kind_cd()[0];

		String str = getSearchDate("s_date") + " ~ " + getSearchDate("e_date");
		str += " / " + upr;
		if (!org.equals("선택") && !org.equals("전체"))
			str += " " + org;
		if (!shelter.equals("선택") && !shelter.equals("전체"))
			str += " " + shelter;

		str += " / " + up_kind;
		if (!kind.equals("선택") && !kind.equals("전체"))
			str += " " + kind;
		
		if (shouldListProtectedOnly())
			str += " / 보호중인 동물만";

		str = str.replaceAll("  +", " ");

		return str;
	}

	public static Uri getUri() {
		String base_url = getApp().getResources().getString(
				shouldListProtectedOnly() ? R.string.protection_list_url
						: R.string.public_list_url);

		Uri uri = Uri.parse(base_url);
		Uri.Builder builder = uri.buildUpon();

		for (String str : new String[] { "s_upr_cd", "s_org_cd",
				"s_shelter_cd", "s_up_kind_cd", "s_kind_cd" }) {
			String[] values = PreferenceManager
					.getDefaultSharedPreferences(getApp()).getString(str, "")
					.split(",");

			String value;
			if (values.length <= 1 || values[1].equals("0000000")
					|| values[1].equals("null"))
				value = str.equals("s_org_cd") ? "0000000" : "";
			else
				value = values[1];

			builder = builder.appendQueryParameter(str, value);
		}

		builder = builder.appendQueryParameter("s_date",
				getSearchDate("s_date"));
		builder = builder.appendQueryParameter("e_date",
				getSearchDate("e_date"));

		uri = builder.build();

		return uri;
	}

	public static String getSearchDate(String key) {
		String date = PreferenceManager.getDefaultSharedPreferences(getApp())
				.getString(key, "");
		if (shouldSearch3Month() || date.equals("")) {
			Calendar cal = Calendar.getInstance();
			if (key.equals("s_date"))
				cal.add(Calendar.MONTH, -3);
			date = String
					.format("%04d-%02d-%02d", cal.get(Calendar.YEAR),
							cal.get(Calendar.MONTH) + 1,
							cal.get(Calendar.DAY_OF_MONTH));
			PreferenceManager.getDefaultSharedPreferences(getApp()).edit()
					.putString(key, date).commit();
		}
		return date;
	}

	public static boolean shouldSearch3Month() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("search_3month", true);
	}

	public static boolean isDebugMode() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("debug_mode", false);
	}

	public static boolean shouldListProtectedOnly() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("list_protected_only", false);
	}

	public static boolean shouldRefresh() {
		return PreferenceManager.getDefaultSharedPreferences(MyApp.getApp())
				.getBoolean("list_refresh", true);
	}

	public static void tweet() {
		// twitter

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
				.setOAuthConsumerKey("RTuqeHwcItbmk1uk4S0HQ")
				.setOAuthConsumerSecret(
						"21lDMch9ce19viGJen0c4JAAkXieN2cjRCeuMD2s")
				.setOAuthAccessToken(
						"77986716-ZWmhOhQG8Smbps0OWh6X84XbRfbwcklc9nD9eRmAM")
				.setOAuthAccessTokenSecret(
						"BncFNV1PYc2SvwpIP3dVnjZ3yoQ4ZRUUZkoYf78Uns");
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		try {
			Query query = new Query("@weird_hat");
			QueryResult res;
			do {
				res = twitter.search(query);
				List<twitter4j.Status> tweets = res.getTweets();
				for (twitter4j.Status tweet : tweets) {
					Log.e("tweet", "@" + tweet.getUser().getScreenName()
							+ " - " + tweet.getText());
				}
			} while ((query = res.nextQuery()) != null);
		} catch (TwitterException te) {
			te.printStackTrace();
			Log.e("tweet", "Failed to search tweets: " + te.getMessage());
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();

		app = this;
	}
}
