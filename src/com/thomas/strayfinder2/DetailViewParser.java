package com.thomas.strayfinder2;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.os.AsyncTask;

class DetailViewParser extends AsyncTask<String, ParsedItem, Exception> {
	class DescItem extends ParsedItem {
		private final String key;
		private final String value;

		public DescItem(String k, String v) {
			key = k;
			value = v;
		}

		@Override
		public String toString() {
			if (key.equals("전화번호"))
				return key + ": <b><a href=\"tel:" + value + "\">" + value
						+ "</a> <- 클릭하면 전화를 걸 수 있습니다.</b><br>";
			else
				return key + ": " + value + "<br>";
		}
	}

	class ImageItem extends ParsedItem {
		private String imgpath;

		public ImageItem(String imgpath) {
			if (imgpath.startsWith("/"))
				this.imgpath = "http://www.animal.go.kr" + imgpath;
			else
				this.imgpath = imgpath;
		}

		@Override
		public String toString() {
			return "<img src=\"" + imgpath + "\"><br><br>";
		}
	}

	@Override
	protected Exception doInBackground(String... params) {
		String view_url = params[0];
		String base_url = params[1];

		try {
			Document doc = Jsoup.parse(new URL(view_url).openConnection()
					.getInputStream(), "euc-kr", base_url);

			// publish image
			String imgpath = base_url
					+ doc.getElementsByAttributeValue("class", "photoArea")
							.first().attr("src");

			publishProgress(new ImageItem(imgpath));

			// publish current status
			publishProgress(new DescItem("상태", params[2]));

			// publish descriptions
			Elements ths = doc.getElementsByTag("th");
			for (Element th : ths)
				publishProgress(new DescItem(th.text(), th.nextElementSibling()
						.text()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return e;
		} catch (IOException e) {
			e.printStackTrace();
			return e;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return e;
		}
		return null;
	}
}

class ParsedItem {

}